//
//  NowPlayingViewModel.swift
//  movie-app-mvvm
//
//  Created by Ali Şengür on 8.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



final class NowPlayingViewModel {
    
    
    var nowPlayingMovies: [Movie] = []
    var reloadData: (() -> Void)?
    
    
    //MARK: - get movies from api
    func getMovies() {
        APIService.shared.getNowPlayingMovies { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let movies):
                self?.nowPlayingMovies = movies
                DispatchQueue.main.async {
                    self?.reloadData!()
                }
            }
        }
    }
}
