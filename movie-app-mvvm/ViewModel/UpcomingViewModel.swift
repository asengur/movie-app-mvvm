//
//  UpcomingViewModel.swift
//  movie-app-mvvm
//
//  Created by Ali Şengür on 8.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation




final class UpcomingViewModel {
    
    var upcomingMovies: [Movie] = [] { /// reload data when change prop
        didSet {
            self.reloadData!()
        }
    }
    
    
    var reloadData: (() -> Void)?
    
    
    //MARK: - get movies from api
    func getMovies() {
        APIService.shared.getUpcomingMovies { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let movies):
                self?.upcomingMovies = movies
            }
        }
    }
    
}

