//
//  MovieDetailViewModel.swift
//  movie-app-mvvm
//
//  Created by Ali Şengür on 8.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



final class MovieDetailViewModel {
    
    
    var movie: Movie? {  // update UI when changed prop
        didSet {
            self.setupUI!()
        }
    }
    
    var similarMovies: [Movie]? { // reload data when changed prop
        didSet {
            self.reloadData!()
        }
    }
    
    
    var setupUI: (() -> Void)?
    var reloadData: (() -> Void)?
    
    
    
    func getMovieFromId(id: Int) {
        APIService.shared.getMovieFromId(id: id) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let movie):
                self?.movie = movie
            }
        }
    }
    
    
    func getSimilarMovies(id: Int) {
        APIService.shared.getSimilarMovies(id: id) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let movie):
                self?.similarMovies = movie
            }
        }
    }
}
