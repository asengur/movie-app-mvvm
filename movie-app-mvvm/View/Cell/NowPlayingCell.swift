//
//  NowPlayingCell.swift
//  movie-app-mvvm
//
//  Created by Ali Şengür on 8.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Kingfisher


class NowPlayingCell: UICollectionViewCell {
    
    //MARK: - outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    //MARK: - configure cell
    public func configure(with movie: Movie) {
        let path = "https://image.tmdb.org/t/p/original\(movie.backdropPath!)"
        let url = URL(string: path)
        
        DispatchQueue.main.async {
            self.imageView.kf.setImage(with: url)
            self.titleLabel.text = "\(movie.title!)(\(movie.yearText))"
        }
    }
    
}
