//
//  UpcomingCell.swift
//  movie-app-mvvm
//
//  Created by Ali Şengür on 8.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Kingfisher


class UpcomingCell: UITableViewCell {

    
    //MARK: - outlets
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        posterImageView.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: - configure cell
    public func configure(with movie: Movie) {
        let path = "https://image.tmdb.org/t/p/original\(movie.posterPath!)"
        let url = URL(string: path)
        
        DispatchQueue.main.async {
            self.posterImageView.kf.setImage(with: url)
            self.titleLabel.text = movie.title
            self.overviewLabel.text = movie.overview
            self.dateLabel.text = movie.releaseText
        }
    }
    
}
