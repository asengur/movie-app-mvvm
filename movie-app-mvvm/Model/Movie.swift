//
//  Movie.swift
//  movie-app-mvvm
//
//  Created by Ali Şengür on 8.10.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



struct MovieResult: Codable {
    let page: Int?
    let numResults: Int?
    let numPages: Int?
    let movies: [Movie]?
    
    private enum CodingKeys: String, CodingKey {
        case page, numResults = "total_results", numPages = "total_pages", movies = "results"
    }
}



//MARK: - Movie details
struct Movie: Codable {
    let id: Int?
    let imdbId: String?
    let title: String?
    let releaseDate: String?
    let voteAverage: Double?
    let overview: String?
    let posterPath: String?
    let backdropPath: String?
    
    
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"
        return dateFormatter
    }()
    
    static private let yearFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        return formatter
    }()
    
    static private let newDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yy"
        return dateFormatter
    }()
    
    
    var yearText: String {
        guard let releaseDate = self.releaseDate, let date = Movie.dateFormatter.date(from: releaseDate) else {
            return ""
        }
        return Movie.yearFormatter.string(from: date)
    }
    
    var releaseText: String {
        guard let releaseDate = self.releaseDate, let date = Movie.dateFormatter.date(from: releaseDate) else {
            return ""
        }
        return Movie.newDateFormatter.string(from: date)
    }
    
    
    var ratingText: String {
        let rating = Int(voteAverage ?? 0.0)
        let ratingText = (0..<rating).reduce("") { (acc, _) -> String in
            return acc + "⭐️"
        }
        return ratingText
    }
    
    
    
    private enum CodingKeys: String, CodingKey {
        case id
        case imdbId = "imdb_id"
        case title
        case releaseDate = "release_date"
        case voteAverage = "vote_average"
        case overview
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
    }
}



//MARK: - Similar Movies
struct SimilarMovies: Codable {
    let page: Int
    let similarMovies: [Movie]?
    
    private enum CodingKeys: String, CodingKey {
        case page, similarMovies = "results"
    }
}
