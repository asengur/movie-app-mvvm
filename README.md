Bu proje **MVVM** mimarisi kullanılarak geliştirildi. 

Kullanılan diğer araç ve kütüphaneler;    
-**Alamofire**  
-**Kingfisher**  
-**Collection View**  
-**Table View**  
-**Page Control**  
-**Search Bar**  
-**Safari Services**    



![home.png](https://bitbucket.org/asengur/movie-app-mvvm/raw/master/movie-app-mvvm/Screenshots/home.png)
![detail.png](https://bitbucket.org/asengur/movie-app-mvvm/raw/master/movie-app-mvvm/Screenshots/details.png)
![search.png](https://bitbucket.org/asengur/movie-app-mvvm/raw/master/movie-app-mvvm/Screenshots/search.png)
![imdbPage.png](https://bitbucket.org/asengur/movie-app-mvvm/raw/master/movie-app-mvvm/Screenshots/imdbPage.png)